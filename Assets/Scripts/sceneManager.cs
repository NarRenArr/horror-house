﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sceneManager : MonoBehaviour
{
    int attempts = 0;

    public SoundManager sound;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(1);
            attempts++;
        }
    }

    public void LoadScene(int index)
    {
        if(sound)
        sound.Audiobutton.Play();
        SceneManager.LoadScene(index);
    }
}
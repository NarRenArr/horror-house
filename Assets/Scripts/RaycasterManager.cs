﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using TMPro;
//using UnityEditorInternal;
using UnityEngine;
using UnityEngine.UI;

public class RaycasterManager : MonoBehaviour
{
    [Header("Сюда текст который будет отображаться")]
    public TextMeshProUGUI KeyTake;
    public TextMeshProUGUI NoKey;
    [Header("Сюда иконки ключей")]
    public Image Key1, Key2, Key3;
    public Image WinImage;
    public Image Dot;
    public EnemyController Enemy1, Enemy2, Enemy3;
    public bool keyGreen, keyRed, keyBlue;
    bool ss;
    Ray ray;
    Ray ray1;
    RaycastHit hit;
    RaycastHit hit1;

    string[] tegs = new string[] { "Key1", "Key2", "Key3", "Door1", "Door2", "Door3", "DoorOpen" };

    Animator door;
    public DoorManager doorManager;
    SoundManager Sound;
    AudioSource audioSource;
    [Header("Дистанция на которой тригерится объект")]
    public float RayDistance;
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        Sound = GetComponent<SoundManager>();
    }
    private void Update()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        ray1 = new Ray(transform.position, Vector3.down);
        if (Physics.Raycast(ray, out hit, RayDistance))
        {
            if (!hit.transform.GetComponent<DoorManager>())
            {
                if (doorManager)
                {
                    doorManager.doorClose.gameObject.SetActive(false);
                    doorManager.doorOpen.gameObject.SetActive(false);
                }
                
            }
            Dot.gameObject.SetActive(tegs.Contains(hit.transform.tag));
            if (hit.transform.CompareTag("Key1"))
            {
                KeyTake.gameObject.SetActive(true);
                if (Input.GetKeyDown(KeyCode.E))
                {
                    keyGreen = true;
                    Sound.Audiokey.Play();
                    Destroy(hit.transform.gameObject);
                    KeyTake.gameObject.SetActive(false);
                    Key1.gameObject.SetActive(true);
                    Debug.Log("Key1");
                }
            }
            if (hit.transform.CompareTag("Key2"))
            {
                KeyTake.gameObject.SetActive(true);
                if (Input.GetKeyDown(KeyCode.E))
                {
                    Sound.Audiokey.Play();
                    Destroy(hit.transform.gameObject);
                    keyRed = true;
                    KeyTake.gameObject.SetActive(false);
                    Key2.gameObject.SetActive(true);
                    Debug.Log("Key2");
                }
            }
            if (hit.transform.CompareTag("Key3"))
            {
                KeyTake.gameObject.SetActive(true);
                if (Input.GetKeyDown(KeyCode.E))
                {
                    Sound.Audiokey.Play();
                    keyBlue = true;
                    Destroy(hit.transform.gameObject);
                    KeyTake.gameObject.SetActive(false);
                    Key3.gameObject.SetActive(true);
                    Debug.Log("Key3");
                }
            }
            if (hit.transform.CompareTag("Door1") && keyGreen)
            {
                door = hit.transform.GetComponent<Animator>();
                doorManager = hit.transform.GetComponent<DoorManager>();
                if (doorManager)
                {
                    if (doorManager.open)
                    {
                        door.SetBool("isOpen", true);
                        hit.transform.GetComponent<BoxCollider>().isTrigger = true;
                        doorManager.doorOpen.gameObject.SetActive(false);
                        doorManager.doorClose.gameObject.SetActive(true);
                    }
                    else
                    {
                        doorManager.doorOpen.gameObject.SetActive(true);
                        doorManager.doorClose.gameObject.SetActive(false);
                        hit.transform.GetComponent<BoxCollider>().isTrigger = false;
                        door.SetBool("isOpen", false);
                    }
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        Sound.Audiodoor.Play();
                        doorManager.open = !doorManager.open;
                    }
                }
                
            }
            if (hit.transform.CompareTag("Door1") && !keyGreen)
            {
                NoKey.gameObject.SetActive(true);
            }
            if (hit.transform.CompareTag("Door2") && keyRed)
            {
                door = hit.transform.GetComponent<Animator>();
                doorManager = hit.transform.GetComponent<DoorManager>();
                if (doorManager)
                {
                    if (doorManager.open)
                    {
                        door.SetBool("isOpen", true);
                        hit.transform.GetComponent<BoxCollider>().isTrigger = true;
                        doorManager.doorOpen.gameObject.SetActive(false);
                        doorManager.doorClose.gameObject.SetActive(true);
                    }
                    else
                    {
                        doorManager.doorOpen.gameObject.SetActive(true);
                        doorManager.doorClose.gameObject.SetActive(false);
                        hit.transform.GetComponent<BoxCollider>().isTrigger = false;
                        door.SetBool("isOpen", false);
                    }
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        Sound.Audiodoor.Play();
                        doorManager.open = !doorManager.open;
                    }
                }
                
            }
            if (hit.transform.CompareTag("Door2") && !keyRed)
            {
                NoKey.gameObject.SetActive(true);
            }
            if (hit.transform.CompareTag("Door3") && keyBlue)
            {
                door = hit.transform.GetComponent<Animator>();
                doorManager = hit.transform.GetComponent<DoorManager>();
                if (doorManager)
                {
                    if (doorManager.open)
                    {
                        door.SetBool("isOpen", true);
                        hit.transform.GetComponent<BoxCollider>().isTrigger = true;
                        doorManager.doorOpen.gameObject.SetActive(false);
                        doorManager.doorClose.gameObject.SetActive(true);
                    }
                    else
                    {
                        doorManager.doorOpen.gameObject.SetActive(true);
                        doorManager.doorClose.gameObject.SetActive(false);
                        hit.transform.GetComponent<BoxCollider>().isTrigger = false;
                        door.SetBool("isOpen", false);
                    }
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        Sound.Audiodoor.Play();
                        doorManager.open = !doorManager.open;
                    }
                }
                
            }
            if (hit.transform.CompareTag("DoorOpen"))
            {
                door = hit.transform.GetComponent<Animator>();
                doorManager = hit.transform.GetComponent<DoorManager>();
                if (doorManager)
                {
                    if (doorManager.open)
                    {
                        door.SetBool("isOpen", true);
                        hit.transform.GetComponent<BoxCollider>().isTrigger = true;
                        doorManager.doorOpen.gameObject.SetActive(false);
                        doorManager.doorClose.gameObject.SetActive(true);
                    }
                    else
                    {
                        doorManager.doorOpen.gameObject.SetActive(true);
                        doorManager.doorClose.gameObject.SetActive(false);
                        hit.transform.GetComponent<BoxCollider>().isTrigger = false;
                        door.SetBool("isOpen", false);
                    }
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        Sound.Audiodoor.Play();
                        doorManager.open = !doorManager.open;
                    }
                }
            }

            if (hit.transform.CompareTag("Door3") && !keyBlue)
            {
                NoKey.gameObject.SetActive(true);
            }
        }
        else
        {
            Debug.Log(doorManager);

            if (doorManager)
            {
                doorManager.doorClose.gameObject.SetActive(false);
                doorManager.doorOpen.gameObject.SetActive(false);
            }
            KeyTake.gameObject.SetActive(false);
            NoKey.gameObject.SetActive(false);
        }
        if(Physics.Raycast(ray1, out hit1, 3f))
        {
            if (hit1.transform.CompareTag("Corridor1"))
            {
                ss = false;
                Enemy1.Tracking = true;
            }
            if (hit1.transform.CompareTag("Corridor2"))
            {
                ss = false;
                Enemy2.Tracking = true;
            }
            if (hit1.transform.CompareTag("Corridor3"))
            {
                ss = false;
                Enemy3.Tracking = true;
            }
            if (hit1.transform.CompareTag("Room"))
            {
                ss = true;
                Enemy1.Tracking = false;
            }
            if (hit1.transform.CompareTag("Room2"))
            {
                ss = true;
                Enemy2.Tracking = false;
            }
            if (hit1.transform.CompareTag("Room3"))
            {
                ss = true;
                Enemy3.Tracking = false;
            }
            if (hit1.transform.CompareTag("Finish"))
            {
                WinImage.gameObject.SetActive(true);
                ss = true;
                Enemy3.Tracking = false;
            }
            if (ss == true)
            {
                Sound.Audiopanicbreathing.mute = true;
                Sound.Audiobreath.mute = false;
            }
            else
            {
                Sound.Audiopanicbreathing.mute = false;
                Sound.Audiobreath.mute = true;
            }
        }
    }
}
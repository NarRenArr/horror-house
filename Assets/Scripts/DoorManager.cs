﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DoorManager : MonoBehaviour
{
    public bool open;
    public TextMeshProUGUI doorOpen;
    public TextMeshProUGUI doorClose;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioSource Audiobreath;
    public AudioSource Audiopanicbreathing;
    public AudioSource Audiowalking;
    public AudioSource Audiodoor;
    public AudioSource Audiofastsphere;
    public AudioSource Audiosphere;
    public AudioSource Audiokey;
    public AudioSource Audiowin;
    public AudioSource Audiolose;
    public AudioSource Audiobutton;
}

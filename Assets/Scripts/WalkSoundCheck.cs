﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkSoundCheck : MonoBehaviour
{
    SoundManager sound;
    bool check;
    private void Start()
    {
        sound = GetComponent<SoundManager>();
        sound.Audiowalking.Pause();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            check = true;
        }
        else
            check = false;
        if (check == true)
        {
            sound.Audiowalking.UnPause();
        }
        else
        {
            sound.Audiowalking.Pause();
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour
{
    public Transform[] points;
    int randomPoints;
    float tresshold = 0.1f;
    public bool Tracking;
    public GameObject Player;
    public Image DeadImage;

    public SoundManager Sound;


    public NavMeshAgent agent;

    private void Start()
    {
        randomPoints = UnityEngine.Random.Range(0, points.Length);
        agent = GetComponent<NavMeshAgent>();
        StartCoroutine(SpeedEnemy());
    }

    private void Update()
    {
        if (!Tracking)
        {
            agent.SetDestination(points[randomPoints].position);
            if (agent.pathStatus == NavMeshPathStatus.PathComplete && agent.remainingDistance < tresshold)
            {
                randomPoints = UnityEngine.Random.Range(0, points.Length);
            }
        }
        else
        {
            agent.SetDestination(Player.transform.position);
        }
        
    }

    IEnumerator SpeedEnemy()
    {
        while (true)
        {
            agent.speed = 1;
            yield return new WaitForSeconds(UnityEngine.Random.Range(30,60));
            Sound.Audiofastsphere.Play();
            agent.speed = 4;
            yield return new WaitForSeconds(UnityEngine.Random.Range(1,4));
            Sound.Audiofastsphere.Stop();
            agent.speed = 1;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Sound.Audiolose.Play();
            DeadImage.gameObject.SetActive(true);
        }
    }
}

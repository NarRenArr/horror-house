﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class LampController : MonoBehaviour
{
    public bool CheckActive;
    [Header("Сюда текст который будет отображаться")]
    public TextMeshProUGUI LampOn, LampOff;

    public void Lamp()
    {
        if (!CheckActive)
        {
            CheckActive = true;
            Debug.Log("On");
            //gameObject.GetComponent<Light>().intensity = 1;
        }
        else
        {
            CheckActive = false;
            Debug.Log("Off");
            //gameObject.GetComponent<Light>().intensity = 0;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnKey : MonoBehaviour
{
    [Header("Сюда точки спавна ключей")]
    public List<GameObject> SpawnKeys = new List<GameObject>();
    [Header("Сюда сами ключики")]
    public GameObject Key1;


    private void Start()
    {
        GenerateKey1();
    }

    public void GenerateKey1()
    {
        int index = Random.Range(0, SpawnKeys.Count);
        Vector3 pos = new Vector3( SpawnKeys[index].transform.position.x, SpawnKeys[index].transform.position.y, SpawnKeys[index].transform.position.z);
        Instantiate(Key1, pos, Key1.transform.rotation);
        SpawnKeys.RemoveAt(index);
    }
}
